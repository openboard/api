import {
  IClientPacketOptions,
  ICloudAppResponse,
  ICorePacket,
  ICredentials,
  ISpec,
  IUser,
  IVersionResponse,
  Platform,
} from "./interfaces";

import { btoa } from "./utils";
import { EventEmitter } from "events";
import { convertUrlToWebsocketUrl, WebSocketClient } from "./wsClient";
import { Logger } from ".";
import fetch from "isomorphic-fetch";

import packagejson from "../package.json";

export interface APIConfig {
  hostname?: string;
  secure?: boolean;
  wsport?: number;
  httpport?: number;
  urlpath?: string;
  apikey?: string[];
  username?: string;
  password?: string;
  wsurl?: string;
  debug?: boolean;
}

export class API {
  apiVersion = packagejson.version;
  hostname: string = "";
  httpport: number = 3000;
  urlpath: string = "";
  servers = [];
  Authorization: string = "";
  apikey?: string;

  /** show debug messages */
  debug = true;

  public onwsdata = (data: { state: ICorePacket; packet: ICorePacket }) => {};

  events = new EventEmitter();
  ws?: WebSocketClient;
  // ws = new WebSocketClient(`ws://${this.hostname}:${this.httpport}`, this);
  // ws = new WebSocketClient((window !== undefined) ? `ws://${window.location.host}` : '', this);
  // ws = new WebSocketClient('', this);

  data: any = {};
  subscriptions: any = [];
  config:APIConfig;

  constructor(config: APIConfig) {
    this.config = config;
    this.loadSession();
    console.log(`${packagejson.name} ${packagejson.version}`);
    if (!config) return;
    if (config.debug !== undefined) this.debug = config.debug;
    // let wsurl = config?.wsurl || 'ws://localhost:3000';
    
    // this.getServers();
    // this.startSocket();
  }

  loadSession = async (): Promise<IUser | void> => {
    let user: IUser;
    /* global localStorage */
    const platform = await this.getPlatform();
    console.log(`Running in ${platform}`);
    this.log("Restoring session...");
    if (platform === "browser") {
      const accountString = await localStorage.getItem("account");
      if (accountString) {
        user = await this.setUser(accountString);
        return user;
      }
    }
  };

  connectWebSocket = async () => {

    const platform = await this.getPlatform();

    if (platform === 'browser') {
      this.config.wsurl = convertUrlToWebsocketUrl(window.location.origin);
    }
    
    // this.config.wsurl = "ws://localhost:5000";

    this.log('api.ts connectWebSocket...')
    console.log(this.config);
    if (this.config.wsurl !== undefined) {
      this.log('connecting websocket...')
      this.ws = new WebSocketClient(this.config.wsurl, this);
    } else {
      this.log('wsurl is not set...')
    }
  }

  // config = (config?: APIConfig) => {
  //   if (config) {
  //     if (config.hostname && config.httpport) {
  //       let protocol = config.secure ? "https" : "http";
  //       this.urlpath = `${protocol}://${config.hostname}:${config.httpport}`;
  //     }

  //     if (config.apikey) {
  //       this.apikey = config.apikey[0];
  //       this.Authorization = "Basic " + btoa("api:key-" + config.apikey[0]);
  //     }

  //     // if (config.urlpath) this.urlpath = config.urlpath;

  //     // this.ws = new WebSocketClient(`ws://${config.hostname}:${config.httpport}`, this);
  //   }
  // };

  setUser = async (accountIn: IUser | string): Promise<IUser> => {
    let account: IUser;

    // if we pass in a string from localStorage etc..
    if (typeof accountIn === "string") {
      const parsedAccount = JSON.parse(accountIn);
      if (!parsedAccount || !parsedAccount.apikey || !parsedAccount.apikey[0])
        throw new Error("account error");
      account = parsedAccount;
    } else {
      account = accountIn;
    }

    if (!account) throw new Error("account error");
    this.data.account = account;
    if (account.apikey && account.apikey[0]) {
      this.apikey = account.apikey[0];
      this.Authorization = "Basic " + btoa("api:key-" + account.apikey[0]);
      this.data.account = account;
    }
    return account;
  };

  async register(credentials: ICredentials): Promise<ICloudAppResponse<IUser>> {
    return fetch(this.urlpath + "/api/v1/register", {
      headers: { "content-type": "application/json" },
      method: "post",
      body: JSON.stringify(credentials),
    }).then((res) => res.json());
  }

  async signin(credentials: ICredentials): Promise<ICloudAppResponse<IUser>> {
    this.log("attempting signin");
    this.log(this.urlpath + "/api/v1/signin");
    return await fetch(this.urlpath + "/api/v1/signin", {
      headers: { "content-type": "application/json" },
      method: "post",
      body: JSON.stringify(credentials),
    })
      .then((res) => res.json() as Promise<ICloudAppResponse<IUser>>)
      .then((response) => {
        if (response && response.is_successful && response.data) {
          let user = response.data;
          if (user && user.apikey && user.apikey[0]) {
            this.setUser(user);
          }
        }
        return response;
      });
  }

  async account(): Promise<ICloudAppResponse<IUser>> {
    return await fetch(this.urlpath + "/api/v1/account", {
      headers: {
        Authorization: this.Authorization,
        "content-type": "application/json",
      },
      method: "get",
    })
      .then((res) => res.json())
      .then((r) => {
        return r;
      })
      .catch((err) => {
        return err;
      });
  }

  /** TODO query type? function from old prototype api.
   * retrieve a single device state in detail.
   */
  async state(query: any): Promise<ICloudAppResponse<ICorePacket>> {
    return await fetch(this.urlpath + "/api/v1/state", {
      headers: {
        Authorization: this.Authorization,
        "content-type": "application/json",
      },
      method: "post",
      body: JSON.stringify(query),
    }).then((res) => res.json());
  }

  // view state of a device by id
  //
  async view(query: any) {
    return this.state(query);
  }

  /** TODO from prototype api.
   * could be combined into /post ?
   */
  async stateupdate(data: any): Promise<ICloudAppResponse<any>> {
    // return this.post(data); // potentially?
    return fetch(this.urlpath + "/api/v1/stateupdate", {
      headers: {
        Authorization: this.Authorization,
        "content-type": "application/json",
      },
      method: "post",
      body: JSON.stringify(data),
    }).then((res) => res.json());
  }

  /** Todo options type? function from old prototype
   * provides all device states in an array
   */
  async states(query?: any): Promise<ICloudAppResponse<ICorePacket[]>> {
    if (query) {
      return fetch(this.urlpath + "/api/v1/states", {
        headers: {
          Authorization: this.Authorization,
          "content-type": "application/json",
        },
        method: "post",
        body: JSON.stringify(query),
      }).then((res) => res.json());
    } else {
      return fetch(this.urlpath + "/api/v1/states", {
        headers: {
          Authorization: this.Authorization,
          "content-type": "application/json",
        },
        method: "get",
      }).then((res) => res.json());
    }
  }

  async post(
    packet: ICorePacket | any
  ): Promise<ICloudAppResponse<ICorePacket>> {
    return fetch(this.urlpath + "/api/v1/data/post", {
      headers: {
        Authorization: this.Authorization,
        "content-type": "application/json",
      },
      method: "post",
      body: JSON.stringify(packet),
    }).then((res) => res.json());
  }

  async packets(
    options: IClientPacketOptions
  ): Promise<ICloudAppResponse<ICorePacket[]>> {
    return await fetch(this.urlpath + "/api/v1/packets", {
      headers: {
        Authorization: this.Authorization,
        "content-type": "application/json",
      },
      method: "post",
      body: JSON.stringify(options),
    })
      .then((res) => res.json())
      .then((r) => {
        return r;
      })
      .catch((err) => {
        return err;
      });
  }

  /////////////////// MOBILE SPECIFIC BELOW

  // async getServers(): Promise<{ urlpath: string, apikey: string[] }[]> {
  //     try {
  //         let servers = await AsyncStorage.getItem('@servers')
  //         if (servers) {
  //             console.log('got servers from storage', servers)
  //             if (isJson(servers)) {
  //                 return JSON.parse(servers);
  //             }
  //         }
  //     } catch (e) {
  //         return []
  //     }
  //     return []
  // }

  // storeServers = async (value: any) => {
  //     try {
  //         await AsyncStorage.setItem('@servers', JSON.stringify(value))
  //     } catch (e) {
  //         console.log('storeServer err', e)
  //         // saving error
  //     }
  // }

  log = new Logger(this).log;

  // log(...data: unknown[]) {
  //     if (this.debug) console.log(`${new Date().toISOString()}\t`, ...data);
  // }

  // updateStateWithPacket(packet: CorePacket) {
  //     this.log('update State With Packet', packet);
  //     this.emit('packet', packet);
  // }

  delete = async (options: { id: string }) => {
    return fetch(this.urlpath + "/api/v1/state/delete", {
      headers: {
        Authorization: this.Authorization,
        "content-type": "application/json",
      },
      method: "delete",
      body: JSON.stringify(options),
    }).then((res) => res.json());
  };

  subscribe = (
    options: { publickey?: string },
    cb?: (state: ICorePacket) => void
  ) => {
    // if (options.username) {
    //   this.search({ username: options.username }, (e, r) => {

    //     if (r) {
    //       let publickey = r.publickey
    //       this.log({ message: "joining publickey", data: { publickey }, level: "verbose", group: "ws" })
    //       this.prototypews.subscribe(r.publickey)
    //     }
    //   })
    // }

    if (options.publickey) {
      this.log({
        message: "joining publickey",
        data: { publickey: options.publickey },
        level: "verbose",
        group: "ws",
      });
      if (this.ws) this.ws.subscribe(options.publickey);

      if (cb) {
        this.subscriptions.push({ publickey: options.publickey, cb });
      }
    }
  };

  /** responds with users details if you have the username, or publickey */
  users = (query: { find: any }): Promise<ICloudAppResponse<IUser[]>> => {
    return fetch(this.urlpath + "/api/v1/users", {
      headers: {
        Authorization: this.Authorization,
        "content-type": "application/json",
      },
      method: "post",
      body: JSON.stringify(query),
    }).then((res) => res.json());
  };

  /** user can search for any text and system responds
   * with matching users, devices, states, groups,
   * boards etc.. */
  search = (query: any): Promise<ICloudAppResponse<any[]>> => {
    return fetch(this.urlpath + "/api/v1/search", {
      headers: {
        Authorization: this.Authorization,
        "content-type": "application/json",
      },
      method: "post",
      body: JSON.stringify(query),
    }).then((res) => res.json());
  };

  /** TODO: implement serverside, copy from prototype server
   * for docs automated documentation
   */
  getapispec = (): Promise<ICloudAppResponse<ISpec[]>> => {
    // "/api/v1"
    return fetch(`${this.urlpath}/api/v1/spec`, {
      headers: {
        Authorization: this.Authorization,
        "content-type": "application/json",
      },
      method: "get",
    }).then((res) => res.json());
  };

  getversion = (): Promise<ICloudAppResponse<IVersionResponse>> => {
    // "/api/v1"
    console.log(this.urlpath);
    return fetch(`${this.urlpath}/api/v1/version`, {
      headers: {
        Authorization: this.Authorization,
        "content-type": "application/json",
      },
      method: "get",
    }).then((res) => res.json());
  };

  getPlatform = async (): Promise<Platform> => {
    if (typeof window === "object") return "browser";
    if (typeof window !== "undefined") return "nodejs";
    return "unknown";
  };
}
