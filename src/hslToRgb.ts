export const hslToRgb = (h: number, s: number, l: number) => {
    let r
    let g
    let b

    if (s === 0) {
        r = g = b = l; // achromatic
    } else {
        const hue2rgb = (p1: number, q1: number, t1: number) => {
            if (t1 < 0) t1 += 1;
            if (t1 > 1) t1 -= 1;
            if (t1 < 1 / 6) return p1 + (q1 - p1) * 6 * t1;
            if (t1 < 1 / 2) return q1;
            if (t1 < 2 / 3) return p1 + (q1 - p1) * (2 / 3 - t1) * 6;
            return p1;
        }

        const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        const p = 2 * l - q;
        r = hue2rgb(p, q, h + 1 / 3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1 / 3);
    }

    return { r: Math.round(r * 255), g: Math.round(g * 255), b: Math.round(b * 255) }
}

