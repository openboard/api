export function mergeDeep(
    // deno-lint-ignore no-explicit-any
    target: Record<string, unknown | any>,
    // deno-lint-ignore no-explicit-any
    source: Record<string, unknown | any>,
) {
    const isObject = (obj: unknown) => obj && typeof obj === "object";

    if (!isObject(target) || !isObject(source)) {
        return source;
    }

    if ((target instanceof Date) || (source instanceof Date)) {
        return source;
    }

    Object.keys(source).forEach((key) => {
        const targetValue = target[key];
        const sourceValue = source[key];

        if (Array.isArray(targetValue) && Array.isArray(sourceValue)) {
            target[key] = targetValue.concat(sourceValue);
        } else if (isObject(targetValue) && isObject(sourceValue)) {
            target[key] = mergeDeep(Object.assign({}, targetValue), sourceValue);
        } else {
            target[key] = sourceValue;
        }
    });

    return target;
}

export function mergeDeepTyped<T>(
    target: T,
    source: T,
) {
    const isObject = (obj: unknown) => obj && typeof obj === "object";

    if (!isObject(target) || !isObject(source)) {
        return source;
    }

    if ((target instanceof Date) || (source instanceof Date)) {
        return source;
    }

    Object.keys(source).forEach((key) => {
        const targetValue = target[key as keyof T];
        const sourceValue = source[key as keyof T];

        if (Array.isArray(targetValue) && Array.isArray(sourceValue)) {
            target[key as keyof T] = targetValue.concat(sourceValue) as any;
        } else if (isObject(targetValue) && isObject(sourceValue)) {
            target[key as keyof T] = mergeDeepTyped<T>(Object.assign({}, targetValue) as any, sourceValue as any) as any;
        } else {
            target[key as keyof T] = sourceValue;
        }
    });

    return target;
}


export function mergeDeepArray<T>(targetArray: T[], sourceData: T, matchPropname: keyof T ): T[] {
    let mergedArray: T[] = [];

    let found = false;
    // search through array if we have this object
    mergedArray = targetArray.map( i => {
        if (i[matchPropname] === sourceData[matchPropname]) {
            // found
            i = mergeDeepTyped<T>(i,sourceData);
            found = true;
        }
        return i;
    })
    // if we do merge data in

    if (!found) {
        mergedArray.push(sourceData);
    }
    // if we dont add it

    return mergedArray;
}