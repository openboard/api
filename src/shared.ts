import { ICorePacket } from "./interfaces";


/**  Removes any non standard characters. */
export function cleanString(str: string) {
    return str.replace(/[^a-z0-9]/gim, "").toLowerCase();
}





/** checks if the input string is a valid email address. */
export function validEmail(input: string) {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(input)
}


export const passwordSettings = { minlength: 5 }

export const usernameSettings = { minlength: 4, maxlength: 15 }

export interface passwordValidation {
    valid: boolean
    capitals: boolean
    lowercase: boolean
    numbers: boolean
    symbols: boolean
    /** does the length match the passwordSettings minimum length? */
    length: boolean,
    /** if the password is free of spaces. true is good, false is bad */
    noSpace: boolean
}

export function validPassword(pw: string) {

    var valid = /[A-Z]/.test(pw) &&
        /[a-z]/.test(pw) &&
        /[0-9]/.test(pw) &&
        /[^A-Za-z0-9]/.test(pw) &&
        (pw.indexOf(" ") < 0) &&
        pw.length >= passwordSettings.minlength;

    return {
        valid,
        capitals: /[A-Z]/.test(pw),
        lowercase: /[a-z]/.test(pw),
        numbers: /[0-9]/.test(pw),
        symbols: /[^A-Za-z0-9]/.test(pw),
        noSpace: (pw.indexOf(" ") < 0),
        length: (pw.length >= passwordSettings.minlength)
    }

}


export function validUsername(username: string) {

    var test = {
        valid: false,
        capitals: /[A-Z]/.test(username),
        lowercase: /[a-z]/.test(username),
        numbers: /[0-9]/.test(username),
        symbols: /[^A-Za-z0-9]/.test(username),
        noSpace: (username.indexOf(" ") < 0),
        length: ((username.length >= usernameSettings.minlength) && (username.length <= usernameSettings.maxlength))
    }

    test.valid = ((!test.capitals) && (test.length) && (test.noSpace) && (!test.symbols));

    return test;
}



export function escapeNonUnicode(str: any) {
    return str.replace(/[^a-z0-9{}"[\]: .,_-]/gim, "");
}


export function ifValidGps(state: ICorePacket) {
    if (!state) return false;
    if (!state.data) return false;
    if (!state.data.gps) return false;
    if (!state.data.gps.lat) return false;
    if (!state.data.gps.lon) return false;
    return [state.data.gps.lat, state.data.gps.lon]
}

