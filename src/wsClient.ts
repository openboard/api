import { isJson } from "./utils";
import { API } from "./api";
import { ICorePacket, Logger } from ".";
import { EventEmitter } from "events";
import WebSocket from "isomorphic-ws";

declare interface OBWSEvents extends EventEmitter {
  on(event: "connect", listener: Function): this;
  on(event: "websocket_state", listener: (state: string) => void): this;
  on(event: "packet", listener: (packet: ICorePacket) => void): this;
  on(event: string, listener: Function): this; 
}

export class WebSocketClient {
  socket?: WebSocket;
  connected = false;
  state = "not_connected";
  api: API;
  waitingForResponse = false;
  keepalive: any;
  pingtimer: any;
  url: string;
  subscriptions: any = [];
  events: OBWSEvents = new EventEmitter();

  constructor(url: string, api: API) {
    this.url = url;
    this.api = api;
    this.connect();

    /* this.keepalive = setInterval(() => {
      console.log('keepalive for websocket...');
      console.log(this.state);
      console.log('websocket sending pingreq');
      this.waitingForResponse = true;
      if (this.socket) {
        this.socket.send(JSON.stringify({cmd: 'pingreq'}));
      }
      clearTimeout(this.pingtimer);
      this.pingtimer = setTimeout(() => {
        console.log('did not get ping pingresponse! reconnecting...');
        this.state = 'not_connected';
        this.api.events.emit('websocket_state', this.state);
        this.connect();
      }, 2000);
    }, 5000);*/
  }

  connect() {
    this.api.log("---- ws state connected:", this.connected);
    if (this.connected) {
      this.api.log("Already connected...");
      return;
    }

    this.state = "not_connected";
    this.waitingForResponse = false;

    this.api.events.emit("websocket_state", this.state);
    this.api.log("connecting websocket", this.url);
    this.socket = new WebSocket(this.url);

    this.socket.onerror = (err) => {
      this.api.log(err);
    };

    this.socket.onopen = () => {
      this.api.log("websocket open");
      if (this.socket) {
        this.socket.send(
          JSON.stringify({ cmd: "connect", apikey: this.api.apikey })
        );
      }
    };

    this.socket.onmessage = (event) => {
      this.api.log("WsClient onMessage", event.data.toString())
      if (!isJson(event.data.toString())) {
        this.api.log("invalid websocket packet from server");
        return;
      }

      let packet = JSON.parse(event.data.toString());

      // api.log("websocket recieved:", packet)

      if (packet.cmd === "connack") {
        this.connected = true;
        if (this.state !== "not_connected") {
          this.api.log("unexpected websocket connack packet");
          return;
        }

        this.api.log("websocket authenticated");
        this.state = "connected";
        this.events.emit("connect");
        this.api.events.emit("websocket_state", this.state);
        // subscribe
        if (this.socket) {
          this.socket.send(
            JSON.stringify({
              cmd: "subscribe",
              subscriptions: [{ apikey: this.api.apikey }],
            })
          );
        }
      }

      if (packet.cmd === "suback") {
        this.api.log("websocket subscribed");
      }

      if (packet.cmd === "publish") {
        let corepacket: ICorePacket = packet.payload;
        // api.log(`ws recv ${corepacket.id}`)
        this.events.emit("packet",corepacket);
        this.api.events.emit("packet", corepacket);
      }

      if (packet.cmd === "pingreq") {
        console.log("websocket pingreq");
        if (this.socket) {
          this.socket.send(JSON.stringify({ cmd: "pingresp" }));
        }
      }

      if (packet.cmd === "pingresp") {
        this.waitingForResponse = false;
        clearTimeout(this.pingtimer);
        this.api.events.emit("websocket_state", this.state);
        console.log("websocket pingresp");
      }

      /*let packet = JSON.parse(event.data);
            // api.log('WEBSOCKET FROM SERVER: ', packet)
            if (packet.message === 'auth') {
                if (props.apikey) if (this.socket) this.socket.send(JSON.stringify({
                    is_successful: true, message: 'apikey', data: { apikey: this.apikey }
                }));
            }

            if (packet.message === 'data') {
                let a: CloudAppResponse<{ state: CorePacket, packet: CorePacket }> = packet.data;
                api.log(JSON.stringify(packet.data, null, 2));
                // this.onwsdata(packet.data);
            }
            */
    };

    //this.socket.send("It worked!")
  }

  /** subscribe TODO */

  log = new Logger(this);

  sendJSON(data: object) {
    if (this.socket) this.socket.send(JSON.stringify(data));
  }

  subscribe(key: string) {
    if (this.connected) {
      console.log("subscribing to " + key);
      if (this.socket) this.socket.send(JSON.stringify({ key }));
    } else {
      this.events.on("connect", () => {
        console.log("subscribing to " + key);
        if (this.socket) this.socket.send(JSON.stringify({ key }));
      });
    }
  }
}

export const convertUrlToWebsocketUrl = (url: string): string => {
  return url.split("http").join("ws") + "/ws";
};
