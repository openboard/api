export type Await<T> = T extends PromiseLike<infer U> ? U : T

export interface ICloudAppResponse<T> {
    is_successful: boolean;
    message: string;
    data?: T;
    exceptionMessage?: string;
    status?: number;
}

/** from package.json properties used in /version api call */
export interface IVersionResponse {
    version: string;
    description: string;
}

export interface ICredentials {
    username: string;
    password: string;
}

export interface IUser {
    /** unique id for this user. used to identify who is the owner of resource */
    userid: string;
    uuid?: string;
    _created_on?: string;
    _last_seen?: string;
    ip?: string;
    userAgent?: string;
    /** encrypted form of password. most instances of users wont even have this encrypted string present. */
    passhash?: string;
    email?: string;
    level: number;
    apikey?: string | string[];
    publickey?: string;
    username: string;
    admin?: boolean;
    emailverified?: boolean;
    /** describes what type of account this is: 
       * undefined == free, can also be "free", "pro", "business", "enterprise" */
    type?: string;
    recover?: {
        code: string;
        timestamp: Date;
    };
}

export type TDateString = string;

export interface ICorePacket {
    id: string;
    /** user.userid that owns this device. Automatically added when posting data. */
    userid?: string;

    type?: string;
    // deno-lint-ignore no-explicit-any
    data?: any;
    apikey?: string;
    alarm?: boolean;
    warning?: boolean;
    shared?: boolean;
    public?: boolean;
    workflow?: string;
    key?: string; // todo: is this secret?
    // deno-lint-ignore no-explicit-any
    [index: string]: any;
    timestamp?: string;
    timestamps?: ITimeStamps;
    layouts?: IDashboardLayouts;
    widgets?: IWidgetType[];
    //display options
    hide?: boolean;
    order?: number;
    name?: string;
    source?: string
}

export interface IDashWidgetLayout {
    i: string;
    x: number;
    y: number;
    w: number;
    h: number;
    moved?: boolean;
    static?: boolean;
}

export interface IDashboardLayouts {
    // xxl?: IDashWidgetLayout[];
    // xl?: IDashWidgetLayout[];
    lg?: IDashWidgetLayout[];
    md?: IDashWidgetLayout[];
    sm?: IDashWidgetLayout[]; 
    // xs?: IDashWidgetLayout[];
    // xxs?: IDashWidgetLayout[];
}

export const IDashboardLayoutsList: IDashboardLayoutsListType[] = [
    // "xxl",
    // "xl",
    "lg",
    "md",
    "sm",
    // "xs",
    // "xxs",
]

export type IDashboardLayoutsListType = keyof IDashboardLayouts;

export interface IWidgetType {
    type: string;
    i: string;
    x?: number;
    y?: number;
    w?: number;
    h?: number;
    /**
     *
     * dot.notation.path.to.nested.data
     *
     *
     * This specific widget may have a datapath assigned to it.      *
     * Usually this is set when you drag and drop an object from the device data panel.
     *
     * eg: root.id
     *
     *      root.data.temperature
     *
     * */
    datapath: string;
    dataname: string;
    /** If any options have been set on this widget */
    options?: any;
}


export interface IWidgetComponentProps {
    /** Settings relevant to this widget */
    widget: IWidgetType
    /** This device full state */
    state: ICorePacket
    /** Data value if drag and dropped from endpoint */
    value?: any

    valueTimestamp?: string;
}

export interface IOptionComponentProps {
    name: string
    option: any
}

export interface IType {
    _id?: any // ObjectId
    type: string;
    widgets: IWidgetType[];
    layouts: IDashboardLayouts;
    workflow?: string;
}



export type ITimeStamps = { [index: string]: string | ITimeStamps };

export interface IDataPostResponse {
    result: "success";
}

/** ------------------------------------------------------- */

/** To utilize the packets api see below: 
 * 
 * @param find mongodb find query object
 * @param sort? (optional) mongodb sort query object
 * @param limit? (optional) number
*/
export interface IClientPacketOptions {
    find: any;
    mask?: any;
    sort?: any;
    limit?: number
    id?: string
    // /** Specify the key of the state you want to retrieve from */
    // key?: CorePacket["key"]
    // /** Specify the datapath of the packet history you want to query
    //  * eg "root.data.temperature"
    //  */
    // datapath?: string
}

/** ------------------------------------------------------- */


export interface IDBchange {
    operationType: "insert" | "replace";
    clusterTime: any;
    fullDocument: ICorePacket;
    ns: { db: string, coll: string }
    documentKey: any
}

export interface IConfigFile {
    ssl: boolean
    sslOptions?: {
        certPath?: string
        keyPath?: string
        caPath?: string
        ca?: any
        key?: any
        cert?: any
    }
    env?: string
    httpPort: number
    httpsPort?: number
    mongoConnection: string
    version: {
        /** package.json name */
        name: string
        /** version of prototype from package.json */
        version: string
        /** description from package.json */
        description: string
    };
}

export interface ICorePacketsOptions {
    request: IClientPacketOptions,
    user: IUser
}

export interface IAPIdocumentation {
    method: string
    path: string
    description?: string
    post?: any
    response?: any
}

/** type for package.json. Can be extended. */
export interface IPackage {
    name: string;
    version: string;
    description: string;
    main: string;
}

export interface ISpec {
    description: string
    method: string
    path: string
    post: string
    response: string
}

export interface IStateUpdate {
    query: any;
    update: any;
}

export type Platform = 'browser' | 'nodejs' | 'unknown';