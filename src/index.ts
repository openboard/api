export * from './interfaces'
export * from './blendrgba'
export * from './hslToRgb'
export * from './api'
export * from './mergeDeep'
export { logger, Logger } from './log'
export * from './utils'
export * from "./shared"
export * from './prototypews'
export * from './wsClient';
export { IDashboardLayoutsList } from './interfaces'

export class OBShared {
    constructor() {
        console.log('OBShared !')
    }
}