import { describe } from "mocha";
import { mergeDeep, mergeDeepTyped, mergeDeepArray } from "./index";

describe("Basic Tests", () => {
  it("mergeDeepArray", async () => {
    var origArray = [{ id: "testA", data: { temp: 25.1 } }, { id: "testB" }];
    var b = { id: "testB", data: { foo: "bar" } };

    var c = mergeDeepArray<any>(origArray, b, "id");

    if (c[1].data.foo != "bar") throw "did not merge correctly";

    if (c.length != 2) throw "did not merge correctly. invalid array length";

    var d = mergeDeepArray<any>(
      c,
      { id: "newDevice", data: { baz: "bar" } },
      "id"
    );

    if (c.length != 2)
      throw "did not merge correctly. invalid array length. expected array length of 2";

    if (d.length != 3)
      throw "did not merge correctly. expected array length of 3";

    var e = mergeDeepArray<any>(
      d,
      { id: "testA", data: { doorOpen: false, temp: 20 } },
      "id"
    );
    
    if (d.length != 3)
      throw "did not merge d correctly. expected array length of 3"; // do not mutate

    if (e.length != 3)
      throw "did not merge e correctly. expected array length of 3"; // do not mutate

    if (e[0].data.doorOpen != false) throw "expected doorOpen property";
    if (e[0].data.temp != 20) throw "expected temp to equal 20";
  });
});
